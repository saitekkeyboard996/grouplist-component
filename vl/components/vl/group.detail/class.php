<?php

use Bitrix\Main\Loader;
use Bitrix\Main;

class GroupDetail extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $arParams['CACHE_TIME'] = intval(trim($arParams["CACHE_TIME"]));
        $arParams['TITLE'] = $arParams['TITLE'];

        return $arParams;
    }

    public function SetTitle($arParams)
    {
        global $APPLICATION;
        if (!empty($arParams)) {
            $APPLICATION->SetTitle($arParams);
        } else {
            $APPLICATION->SetTitle('По умолчанию');
        }
    }

    public function getGroup()
    {
        $select = array('NAME', 'ID', 'C_SORT', 'DESCRIPTION');
        $filter = array
        (
            "ID" => $this->arParams['GROUP_ID'],
        );

        $result = Bitrix\Main\GroupTable::getList(
            array(
                'select' => $select,
                'filter' => $filter,
            )
        );
        while ($arGroup = $result->fetch()) {
            $this->arResult = $arGroup;
        }
    }


    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->SetTitle($this->arParams['TITLE']);
            $this->getGroup();
            $this->IncludeComponentTemplate();
        }
    }

}
