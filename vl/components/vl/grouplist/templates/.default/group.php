<?php

global $APPLICATION;

$APPLICATION->IncludeComponent(
    "vl:group.list",
    "template1",
    array(
        "TITLE" => $arParams['TITLE'],    // Заголовок страницы
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],    // Тип кеширования
        "CACHE_TIME" => $arParams["CACHE_TIME"],    // Время кеширования (сек.)
    ),
    false,
    array(
        "ACTIVE_COMPONENT" => "Y"
    )
); ?>
