<?php
global $APPLICATION;
$APPLICATION->IncludeComponent(
	"vl:group.detail", 
	"template1", 
	array(
		"TITLE" => $arParams["TITLE"],
		"COMPONENT_TEMPLATE" => "template1",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"GROUP_ID" => $arResult["VARIABLES"]["ID"]
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
); ?>
