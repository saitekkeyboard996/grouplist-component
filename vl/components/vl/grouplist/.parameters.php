<?php


$arComponentParameters = [
    // основной массив с параметрами
    "PARAMETERS" => [
        'GROUP_ID'=>[
            "PARENT" => "BASE",
            'NAME'=>GetMessage('GROUP_USER_ID'),
            'DEFAULT'=>'={$_REQUEST["ID"]}',
            "TYPE" => "STRING",
        ],
        // псевдоимена для комплексного компонента
        "VARIABLE_ALIASES" => [
            // элемент
            "ID" => [
                "NAME" => GetMessage("GROUP_USER_ID"),
            ],
        ],
        // настройки режима ЧПУ
        "SEF_MODE" => [
            // настройки для элемента
            "group" => [
                "NAME" => GetMessage("GROUP_USER_ITEMS_LIST"),
                "DEFAULT" => "",
                "VARIABLES" => [],
            ],
            "detail" => [
                "NAME" => GetMessage("GROUP_USER_DETAIL_ITEM"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => ["ELEMENT_ID", "SECTION_ID"],
            ],
        ],

        "CACHE_TIME"  =>  ["DEFAULT"=>36000000],
        'TITLE'=>['PARENT'=>'BASE','TYPE'=>'STRING','NAME'=>GetMessage("GROUP_USER_TITLE_PAGE"),'DEFAULT'=>'По умолчанию'],
    ]
];
