<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("GROUP_USER_NAME"),
    "DESCRIPTION" => GetMessage("GROUP_USERS_DESCRIPTION"),
    "ICON" => "/images/news_all.gif",
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "news",
            "NAME" => GetMessage("GROUP_USER_LIST"),
            "SORT" => 10,
            "CHILD" => array(
                "ID" => "group",
            ),
        ),
    ),
);

?>