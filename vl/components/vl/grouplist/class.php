<?php

/**
 * @var $APPLICATION CMain
 * @var $USER CUser
 */

use Bitrix\Iblock\Component\Tools;
use Bitrix\Main\Loader;
use \Bitrix\Main\Application,
    \Bitrix\Main\Context,
    \Bitrix\Main\Request;

class groupList extends CBitrixComponent
{

    protected array $arDefaultUrlTemplates404 = array(
        "group" => "",
        "detail" => "#ELEMENT_ID#/",
    );
    protected array $arComponentVariables = array(
        "SECTION_ID",
        "ID",
        "SECTION_CODE",
        "ELEMENT_ID",
        "ELEMENT_CODE",
    );
    protected array $arDefaultVariableAliases = array();
    protected $arDefaultVariableAliases404 = array();


    public function onPrepareComponentParams(
        $arParams
    ) {
        $arParams['CACHE_TIME'] = intval(trim($arParams["CACHE_TIME"]));
        $arParams['TITLE'] = $arParams['TITLE'];

        return $arParams;
    }

    public function SetTitle($arParams)
    {
        global $APPLICATION;
        if (!empty($arParams)) {
            $APPLICATION->SetTitle($arParams);
        } else {
            $APPLICATION->SetTitle('По умолчанию');
        }
    }
    protected function SefMode(){
        $arVariables = array();

        $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($this->arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::makeComponentVariableAliases($this->arDefaultVariableAliases404, $this->arParams["VARIABLE_ALIASES"]);

        $engine = new CComponentEngine($this);
        if (CModule::IncludeModule('iblock'))
        {
            $engine->addGreedyPart("#SECTION_CODE_PATH#");
            $engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
        }
        $componentPage = $engine->guessComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );


        $b404 = false;
        if(!$componentPage)
        {
            $componentPage = "group";
            $b404 = true;
        }


        if($b404 && CModule::IncludeModule('iblock'))
        {
            $folder404 = str_replace("\\", "/", $this->arParams["SEF_FOLDER"]);
            if ($folder404 != "/")
                $folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
            if (mb_substr($folder404, -1) == "/")
                $folder404 .= "index.php";
            global $APPLICATION;
            if ($folder404 != $APPLICATION->GetCurPage(true))
            {
                \Bitrix\Iblock\Component\Tools::process404(
                    ""
                    ,($this->arParams["SET_STATUS_404"] === "Y")
                    ,($this->arParams["SET_STATUS_404"] === "Y")
                    ,($this->arParams["SHOW_404"] === "Y")
                    ,$this->arParams["FILE_404"]
                );
            }
        }

        CComponentEngine::initComponentVariables($componentPage, $this->arComponentVariables, $arVariableAliases, $arVariables);

        $this->arResult = array(
            "FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $this->arVariableAliases,
        );
        return $componentPage;


    }
    protected function DefMode(){
        $arVariableAliases = CComponentEngine::makeComponentVariableAliases($this->arDefaultVariableAliases, $this->arParams["VARIABLE_ALIASES"]);
        CComponentEngine::initComponentVariables(false, $this->arComponentVariables, $arVariableAliases, $arVariables);

        $componentPage = "";

        if(isset($arVariables["ID"]) && intval($arVariables["ID"]) > 0){
            $componentPage = "detail";
        }elseif(isset($arVariables["ELEMENT_CODE"]) && $arVariables["ELEMENT_CODE"] <> ''){
            $componentPage = "detail";
        }else{
            $componentPage = "group";
        }

        global $APPLICATION;
        $this->arResult = array(
            "FOLDER" => "",
            "URL_TEMPLATES" => array(
                "group" => htmlspecialcharsbx($APPLICATION->GetCurPage()),
                "detail" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["ELEMENT_ID"]."=#ID#"),
            ),
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases
        );
        return $componentPage;
    }



    public function executeComponent()
    {
        if ($this->startResultCache()) {
            if($this->arParams["SEF_MODE"] == "Y"){

                $componentPage = $this->SefMode();
            }
            if($this->arParams["SEF_MODE"] != "Y"){
                $componentPage = $this->DefMode();
            }
            $this->SetTitle($this->arParams['TITLE']);
            $this->IncludeComponentTemplate($componentPage);
        }
    }

}