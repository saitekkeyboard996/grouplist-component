<?php

$arComponentParameters = [
    // основной массив с параметрами
    "PARAMETERS" => [
        'GROUP_ID'=>[
            "PARENT" => "BASE",
            'NAME'=>GetMessage('GROUP_USER_ID'),
            'DEFAULT'=>'={$_REQUEST["ID"]}',
            "TYPE" => "STRING",
        ],
        'TITLE'=>[
            "PARENT" => "BASE",
            'NAME'=> GetMessage("GROUP_USER_TITLE_PAGE"),
            'DEFAULT'=>'Список групп',
            "TYPE" => "STRING",
        ],
        "CACHE_TIME"  =>  ["DEFAULT"=>36000000],
    ]
];